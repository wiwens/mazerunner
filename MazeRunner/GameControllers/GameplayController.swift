//
//  GameplayController.swift
//  MazeRunner
//
//  Created by Wiktor Wielgus on 02.06.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation
import SpriteKit

class GameplayController {
    
    static let instance = GameplayController()
    private init() {}
    
    var scoreText: SKLabelNode?
    var score: Int32?
    
    func initializeVariables() {
        score = 0
    }
    
    func incrementScore() {
        score! += 1
        scoreText?.text = "\(score!)"
    }
}