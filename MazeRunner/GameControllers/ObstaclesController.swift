//
//  ObstaclesController.swift
//  MazeRunner
//
//  Created by Wiktor Wielgus on 04.06.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class ObstaclesController {
    
    var lastObstaclePositionY = CGFloat(-400)
    
    private func createObstacle(frame: CGRect) -> SKNode {
        
        let leftObstacle = LeftObstacleNode.createLeftObstacleNode(frame)
        let rightObstacle = RightObstacleNode.createLeftObstacleNode(frame)
        let obstaclePair = SKNode()
        obstaclePair.name = "ObstaclePair"
        obstaclePair.addChild(leftObstacle)
        obstaclePair.addChild(rightObstacle)
        obstaclePair.zPosition = 3
        
        let randomPosition = CGFloat.random(min: -frame.size.width / 2 + 20, max: frame.size.width / 2 - 20)
        obstaclePair.position.x = obstaclePair.position.x + randomPosition
        
        return obstaclePair
    }
    
    func arrangeObstacleInScene(scene: SKScene, frame: CGRect) -> CGFloat {
        
        let obstacle = createObstacle(frame)
        
        obstacle.position.y = lastObstaclePositionY
        lastObstaclePositionY -= 200
        
        scene.addChild(obstacle)
        
        return obstacle.position.y
    }
}
