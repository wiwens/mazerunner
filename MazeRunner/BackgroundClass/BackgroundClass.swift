//
//  BackgroundClass.swift
//  MazeRunner
//
//  Created by Wiktor Wielgus on 02.06.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class BackgroundClass: SKSpriteNode {
    
    func moveBG(camera: SKCameraNode) {
        
        if self.position.y - self.size.height - 10 > camera.position.y {
            
            self.position.y -= self.size.height * 3
        }
    }
}
