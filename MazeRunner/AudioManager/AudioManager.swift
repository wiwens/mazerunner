//
//  AudioManager.swift
//  MazeRunner
//
//  Created by Wiktor Wielgus on 06.06.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import AVFoundation

class AudioManager {
    
    static let instance = AudioManager()
    private init() {}
    
    private var audioPlayer: AVAudioPlayer?
    
    func playBGMusic() {
        
        let url = NSBundle.mainBundle().URLForResource("soundtrack", withExtension: "mp3")
        var err: NSError?
        
        do {
            
            try audioPlayer = AVAudioPlayer(contentsOfURL: url!)
            audioPlayer?.numberOfLoops = -1
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
            
        } catch let err1 as NSError {
            err = err1
        }
        
        if err != nil {
            print("We have a problem \(err)")
        }
    }
    
    func isAudioPlayerInitialized() -> Bool {
        
        return audioPlayer == nil
    }
}
