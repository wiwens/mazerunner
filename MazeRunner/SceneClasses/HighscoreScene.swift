//
//  HighscoreScene.swift
//  MazeRunner
//
//  Created by Wiktor Wielgus on 02.06.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class HighscoreScene: SKScene {
    
    private var scoreLabel: SKLabelNode?
    
    override func didMoveToView(view: SKView) {
        getReference()
        setScore()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            
            let location = touch.locationInNode(self)
            
            if nodeAtPoint(location).name == "Back" {
                let scene = MainMenuScene(fileNamed: "MainMenuScene")
                scene!.scaleMode = .AspectFill
                self.view?.presentScene(scene!, transition: SKTransition.doorsCloseVerticalWithDuration(1))
            }
        }
    }
    
    private func getReference() {
        scoreLabel = self.childNodeWithName("ScoreLabel") as? SKLabelNode!
        scoreLabel?.fontName = "MinecraftEvenings"
        scoreLabel?.text = "\(NSUserDefaults.standardUserDefaults().integerForKey("Highscore"))"
    }
    
    private func setScore() {
        
    }
}