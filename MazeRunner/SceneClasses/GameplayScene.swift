//
//  GameScene.swift
//  MazeRunner
//
//  Created by Wiktor Wielgus on 20.04.2016.
//  Copyright (c) 2016 Wiktor. All rights reserved.
//

import SpriteKit
import CoreMotion

class GameplayScene: SKScene, SKPhysicsContactDelegate {
    
    let manager = CMMotionManager()
    
    var mainCamera: SKCameraNode?
    
    var bg1: BackgroundClass?
    var bg2: BackgroundClass?
    var bg3: BackgroundClass?
    
    var player: Player?
    
    private var acceleration = CGFloat()
    private var cameraSpeed = CGFloat()
    private var maxSpeed = CGFloat()
    
    var lastObstaclePosition = CGFloat(-500)
    
    var obstaclesController = ObstaclesController()
    
    var lastYPosition = CGFloat(-200)
    
    var moveAndRemove: SKAction?
    
    private var cameraDistanceBeforeCreatingNewObstacle: CGFloat?
    
    override func didMoveToView(view: SKView) {
        initializeMotion()
        initializeVariables()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            
            if nodeAtPoint(location).name == "Restart" {
                let gameplay = GameplayScene(fileNamed: "GameplayScene")
                gameplay!.scaleMode = .AspectFill
                self.view?.presentScene(gameplay!, transition: SKTransition.doorwayWithDuration(1.5))
            }
            
            if nodeAtPoint(location).name == "Quit" {
                let mainMenu = MainMenuScene(fileNamed: "MainMenuScene")
                mainMenu!.scaleMode = .AspectFill
                self.view?.presentScene(mainMenu!, transition: SKTransition.doorwayWithDuration(1.5))
            }
        }
    }
   
    func initializeVariables() {
        physicsWorld.contactDelegate = self
        
        mainCamera = self.childNodeWithName("MainCamera") as? SKCameraNode!
        
        player = self.childNodeWithName("Player") as? Player!
        
        getBackgrounds()
        getLabels()
        
        GameplayController.instance.initializeVariables()
        
        setCameraSpeed()
        
        cameraDistanceBeforeCreatingNewObstacle = (mainCamera?.position.y)! - 400
    }
    
    func initializeMotion() { 
        manager.startAccelerometerUpdates()
        manager.accelerometerUpdateInterval = 0.1
        manager.startAccelerometerUpdatesToQueue(NSOperationQueue.mainQueue()) {
            (data, error) in
            
            self.physicsWorld.gravity = CGVectorMake(CGFloat((data?.acceleration.x)!) * 20, CGFloat((data?.acceleration.y)!) * 20)
        }
    }
    
    func getBackgrounds() {
        bg1 = self.childNodeWithName("BG 1") as? BackgroundClass!
        bg2 = self.childNodeWithName("BG 2") as? BackgroundClass!
        bg3 = self.childNodeWithName("BG 3") as? BackgroundClass!
    }
    
    func getLabels() {
        GameplayController.instance.scoreText = self.mainCamera!.childNodeWithName("Score") as? SKLabelNode!
        GameplayController.instance.scoreText?.fontName = "MinecraftEvenings"
    }
    
    func moveCamera() {
        cameraSpeed += acceleration
        
        if cameraSpeed > maxSpeed {
            
            cameraSpeed = maxSpeed
        }
        
        self.mainCamera?.position.y -= cameraSpeed
    }
    
    func manageBackgrounds() {
        bg1?.moveBG(mainCamera!)
        bg2?.moveBG(mainCamera!)
        bg3?.moveBG(mainCamera!)
    }
    
    private func setCameraSpeed() {
            acceleration = 0.001
            cameraSpeed = 1.5
            maxSpeed = 8
    }
    
    func checkForChildsOutOffScreen() {
        
        for child in children {
            if child.position.y > (mainCamera?.position.y)! + (self.scene?.size.height)! {
                
                let childName = child.name?.componentsSeparatedByString(" ")
                
                if childName![0] != "BG" {
                    
                    print("The child that was removed is \(child.name!)")
                    child.removeFromParent()
                }
            }
        }
    }
    
    func managePlayer() {
        if (mainCamera?.position.y)! + self.frame.height / 2 + (player?.size.height)! < player?.position.y {
            self.scene?.paused = true
            playerDied()
        }
    }
    
    func playerDied() {
        
        let highscore = NSUserDefaults.standardUserDefaults().integerForKey("Highscore")
        
        if highscore < Int(GameplayController.instance.score!) {
            NSUserDefaults.standardUserDefaults().setInteger(Int(GameplayController.instance.score!), forKey: "Highscore")
        }
        
        for child in children {
            
            let childName = child.name?.componentsSeparatedByString(" ")
            
            if childName![0] != "BG" {
                
                print("The child that was removed is \(child.name!)")
                child.removeFromParent()
            }
        }
        
        createRestartPanel()
    }
    
    func createRestartPanel() {
        let restart = SKSpriteNode(imageNamed: "retryBTN")
        let quit = SKSpriteNode(imageNamed: "back_menuBTN")
        let score = SKLabelNode(text: "\(GameplayController.instance.score!)")
        
        score.fontName = "MinecraftEvenings"
        score.fontSize = 60
        score.position = CGPoint(x: 0, y: (mainCamera?.position.y)! + 200)
        score.zPosition = 12
        
        restart.name = "Restart"
        restart.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        restart.position = CGPoint(x: 0, y: (mainCamera?.position.y)! + 40)
        restart.zPosition = 12
        
        quit.name = "Quit"
        quit.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        quit.position = CGPoint(x: 0, y: (mainCamera?.position.y)! - 60)
        quit.zPosition = 12
        
        self.addChild(restart)
        self.addChild(quit)
        self.addChild(score)
    }
    
    func createObstacle() {
        
        if mainCamera?.position.y < lastObstaclePosition + 500 {
            lastObstaclePosition = obstaclesController.arrangeObstacleInScene(self.scene!, frame: self.frame)
        }
        
        checkForChildsOutOffScreen()
    }
    
    override func update(currentTime: CFTimeInterval) {
        moveCamera()
        managePlayer()
        manageBackgrounds()
        player?.setScore()
        createObstacle()
    }
}
