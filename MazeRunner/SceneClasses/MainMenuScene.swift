//
//  MainMenuScene.swift
//  MazeRunner
//
//  Created by Wiktor Wielgus on 02.06.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class MainMenuScene: SKScene {
    
    override func didMoveToView(view: SKView) {
        setUpMusic()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            
            let location = touch.locationInNode(self)
            
            if let name = nodeAtPoint(location).name {
                switch name {
                case "StartGame":
                    
                    let scene = GameplayScene(fileNamed: "GameplayScene")
                    scene?.scaleMode = .AspectFill
                    self.view?.presentScene(scene!, transition: SKTransition.doorsOpenVerticalWithDuration(1))
                    
                    break
                case "HighScore":
                    
                    let scene = HighscoreScene(fileNamed: "HighscoreScene")
                    scene?.scaleMode = .AspectFill
                    self.view?.presentScene(scene!, transition: SKTransition.doorsOpenVerticalWithDuration(1))
                    
                    break
                default:
                    
                    break
                }
            }
    
        }
    }
    
    func setUpMusic() {
        if AudioManager.instance.isAudioPlayerInitialized() {
            AudioManager.instance.playBGMusic()
        }
    }
    
}
