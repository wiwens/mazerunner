//
//  Player.swift
//  MazeRunner
//
//  Created by Wiktor Wielgus on 02.06.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class Player: SKSpriteNode {
    
    private var lastY = CGFloat(0)
    
    func setScore() {
        
        if self.position.y < lastY {
            
            GameplayController.instance.incrementScore()
            lastY = self.position.y
        }
    }
}
