//
//  LeftObstacle.swift
//  MazeRunner
//
//  Created by Wiktor Wielgus on 03.06.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class  LeftObstacleNode {
    
    class func createLeftObstacleNode(frame: CGRect) -> SKSpriteNode {
        
        let leftObstacle = SKSpriteNode(imageNamed: "Obstacle")
        
        leftObstacle.position = CGPoint(x: -leftObstacle.frame.size.width/2 - 35, y: CGPointZero.y)
        leftObstacle.physicsBody = SKPhysicsBody(rectangleOfSize: leftObstacle.size)
        leftObstacle.physicsBody?.affectedByGravity = false
        leftObstacle.physicsBody?.dynamic = false
        return leftObstacle
    }
    
}
