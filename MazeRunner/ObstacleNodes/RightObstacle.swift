//
//  RightObstacle.swift
//  MazeRunner
//
//  Created by Wiktor Wielgus on 03.06.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class  RightObstacleNode {
    
    class func createLeftObstacleNode(frame: CGRect) -> SKSpriteNode {
        
        let rightObstacle = SKSpriteNode(imageNamed: "Obstacle")
        
        rightObstacle.position = CGPoint(x: rightObstacle.frame.size.width/2 + 35, y: CGPointZero.y)
        rightObstacle.physicsBody = SKPhysicsBody(rectangleOfSize: rightObstacle.size)
        rightObstacle.physicsBody?.affectedByGravity = false
        rightObstacle.physicsBody?.dynamic = false
        return rightObstacle
    }
    
}
